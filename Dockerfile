# Dockerfile for building ansible toolkit


FROM alpine:3.15.0 as builder

MAINTAINER G. K. E. <gke@6admin.io>

ARG VERSION=5.4.0

RUN set -xe && \
    apk update && \
    apk add --no-cache --purge --virtual .build-deps -Uu gcc musl-dev libffi-dev python3 python3-dev py3-pip && \
    python3 -m venv /opt/venv && \
    source /opt/venv/bin/activate && \
    python3 -m pip install --no-cache-dir --upgrade pip setuptools && \
    python3 -m pip install --no-cache-dir --no-compile ansible-core ansible-lint==$VERSION && \
    python3 -m pip uninstall --yes setuptools wheel && \
    find /opt/venv/lib/ -type d -name '__pycache__' -delete && \
    find /opt/venv/lib/ -type f -name '*.pyc' -delete && \
    rm -rf /tmp/* /var/tmp/* && \ 
    apk del .build-deps

FROM alpine:3.15.0 as default

MAINTAINER G. K. E. <gke@6admin.io>

LABEL "maintainer"="G. K. E. <gke@6admin.io>"
LABEL "org.opencontainers.image.authors"="G. K. E. <gke@6admin.io>"
LABEL "org.opencontainers.image.vendor"="6admin"
LABEL "org.opencontainers.image.licenses"="GPL3"
LABEL "org.opencontainers.image.url"="https://gitlab.com/6admin/docker/alpine/ansible-lint"
LABEL "org.opencontainers.image.documentation"="https://gitlab.com/6admin/docker/alpine/ansible-lint"
LABEL "org.opencontainers.image.source"="https://gitlab.com/6admin/docker/alpine/ansible-lint"
LABEL "org.opencontainers.image.ref.name"="Ansible-lint ${VERSION}"
LABEL "org.opencontainers.image.title"="Ansible-lint ${VERSION}"
LABEL "org.opencontainers.image.description"="Ansible-lint ${VERSION}"

COPY --from=builder /opt/venv /opt/venv

ENV PATH="/opt/venv/bin:$PATH"

RUN set -xe && \
    apk update && \
    apk add --no-cache git python3

WORKDIR /src

ENTRYPOINT ["ansible-lint"]
CMD ["--version"]
